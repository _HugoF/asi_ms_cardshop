# ASI_MS_CardShop

Groupe : Contini Enzo / Dugué Dorian / Ferrer Hugo / Gasparini-Barrelon Brian

## Éléments réalisés du cahier des charges
- Application SOA transformée en MS
- Tests unitaires (sur atelier 2) 
- Authentification par token maison (Concaténation de champs + encodage base 64) type session
- Rapports au fil des ateliers : Comparaison MS vs SOA vs MVC (voir atelier3.pdf rendu)

## Éléments non-réalisés du cahier des charges
- Front partiellement fonctionnel (atelier 2)
- Partie Room et combat
- CI (uniquement atelier 2)

## Éléments éventuels réalisés en plus du cahier des charges
- Séquences BDD 
- Énumérations pour entité Card (Family, Affinity)
- Application multi-modules avec une librairie dédiée à chacun de nos MS
- RP NGINX avec routage vers MS authentification pour les APIs exposées + Gestion code erreur retour + Gestion Windows Docker Network

## Pourcentage d'implication de chaque membre du groupe
- Contini Enzo : 25%
- Dugué Dorian : 25%
- Ferrer Hugo : 25%
- Gasparini-Barrelon Brian : 25% 

##  Liens vers projet Gitlab (repo public)
Atelier 1 & 2 : https://github.com/briangasparini/TP_ASI_FERRER_CONTINI_DUGUE_GASPARINI
Atelier 3 : https://gitlab.com/_HugoF/asi_ms_cardshop
