package com.cardshop.ms_auth.service;

import java.util.Base64;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cardshop.authlib.dto.TokenDTO;
import com.cardshop.ms_auth.dtomapper.TokenMapper;
import com.cardshop.ms_auth.model.Token;
import com.cardshop.ms_auth.repository.AuthRepository;
import com.cardshop.userlib.dto.UserDTO;
import com.cardshop.userlib.restclient.UserRestClient;

@Service
public class AuthService {

	@Autowired
	AuthRepository aRepository;
	@Autowired
	UserRestClient uRestClient;

	public UserDTO getUserByLogin(String login) {
		return uRestClient.findByLogin(login);
	}
	
	
	public void addToken(TokenDTO t) {
		aRepository.save(TokenMapper.convertDtoToken(t));
	}
	
	public TokenDTO getTokenByUserId(int userId) {
		Token token = aRepository.findByUserId(userId)
				.orElseThrow(()-> new RuntimeException("Error : Can't get token linked to user : " + userId));
		return TokenMapper.convertTokenDto(token);
	}
	
	public Token getTokenById(int id) {
		Optional<Token> tOpt = aRepository.findById(id);
		if (tOpt.isPresent()) {
			return tOpt.get();
		} else {
			return null;
		}
	}
	
	public TokenDTO getTokenDto(int id) {
		Optional<TokenDTO> tOpt = aRepository.findById(id).map(token -> TokenMapper.convertTokenDto(token));
		if (tOpt.isPresent()) {
			return tOpt.get();
		} else {
			return null;
		}
	}
	public TokenDTO getTokenByToken(String tokenValue) {
		Optional<TokenDTO> tOpt = aRepository.findByToken(tokenValue).map(token -> TokenMapper.convertTokenDto(token));
		if (tOpt.isPresent()) {
			return tOpt.get();
		} else {
			return null;
		}
	}
	public boolean checkToken(String encodedToken) {
		TokenDTO token = this.getTokenByToken(encodedToken);
    	if(token != null) {
    		return true;
    	}
    	return false;
    }
	public String createToken(UserDTO userDto) {
		if (userDto != null) {
			String tokenValue = Base64.getEncoder()
					.encodeToString((userDto.getLogin() + "/" + userDto.getPassword() + "/" + userDto.getId()).getBytes());
			Token token = new Token();
			token.setToken(tokenValue);
			token.setUserId(userDto.getId());
			this.addToken(TokenMapper.convertTokenDto(token));
			return tokenValue;
		} else {
			return null;
		}
	}
	public void eraseToken(TokenDTO t) {
		aRepository.deleteByToken(t.getToken());
	}
}
