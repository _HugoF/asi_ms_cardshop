package com.cardshop.ms_front;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsFrontApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsFrontApplication.class, args);
	}

}
