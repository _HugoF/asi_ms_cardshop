CREATE DATABASE market;
CREATE USER market with encrypted password 'password';
GRANT ALL PRIVILEGES ON DATABASE market to market;