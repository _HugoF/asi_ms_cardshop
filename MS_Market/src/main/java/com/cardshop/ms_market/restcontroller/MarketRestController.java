package com.cardshop.ms_market.restcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cardshop.cardlib.dto.CardDTO;
import com.cardshop.cardlib.restclient.CardRestClient;
import com.cardshop.marketlib.dto.ExchangeDTO;
import com.cardshop.marketlib.dto.MarketDTO;
import com.cardshop.marketlib.restclient.IMarketRest;
import com.cardshop.ms_market.service.MarketService;
import com.cardshop.userlib.dto.UserDTO;
import com.cardshop.userlib.restclient.UserRestClient;

@RestController
public class MarketRestController implements IMarketRest {
	
	@Autowired
	UserRestClient uRestClient;
	
	@Autowired
    MarketService mService;
	
	@Autowired
	CardRestClient cRestClient;
	
	@Override
    @RequestMapping(method=RequestMethod.POST,value="/api/buy")
    public void buyCard(@RequestBody ExchangeDTO exchangeDTO) {
    	UserDTO user = uRestClient.findById(exchangeDTO.getUserId());
    	CardDTO card = cRestClient.findById(exchangeDTO.getCardId());
    	mService.buyCard(user, card);
    }
    
	@Override
    @RequestMapping(method=RequestMethod.POST,value="/api/sell")
    public void sellCard(@RequestBody ExchangeDTO exchangeDTO) {
    	UserDTO user = uRestClient.findById(exchangeDTO.getUserId());
    	CardDTO card = cRestClient.findById(exchangeDTO.getCardId());
    	mService.sellCard(user, card);
    }
	
	@Override
    @RequestMapping(method=RequestMethod.GET,value="/api/transactions")
    public List<MarketDTO> findAll() {
    	return mService.selectMarkets();
    }
	
	@Override
    @RequestMapping(method=RequestMethod.GET,value="/api/transactions/{id}")
    public MarketDTO findById(@PathVariable int id) {
    	return mService.selectMarketById(id);
    }
}
