package com.cardshop.ms_market;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.cardshop.userlib", "com.cardshop.marketlib", "com.cardshop.cardlib" })
public class MsMarketApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsMarketApplication.class, args);
	}

}
