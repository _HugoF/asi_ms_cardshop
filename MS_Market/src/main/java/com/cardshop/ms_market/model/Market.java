package com.cardshop.ms_market.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Market {
	
	@Id
	@GeneratedValue
	private int id;
	private int idBuyer;
	private int idSeller;
	private int idCard;
	
	
	public Market() {
	}
	
	public Market(int id, int id_buyer, int id_seller, int id_card) {
		super();
		this.id = id;
		this.idBuyer = id_buyer;
		this.idSeller = id_seller;
		this.idCard = id_card;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getIdBuyer() {
		return idBuyer;
	}
	
	public void setIdBuyer(int id_buyer) {
		this.idBuyer = id_buyer;
	}
	
	public int getIdSeller() {
		return idSeller;
	}
	
	public void setIdSeller(int id_seller) {
		this.idSeller = id_seller;
	}
	
	public int getIdCard() {
		return idCard;
	}
	
	public void setIdCard(int id_card) {
		this.idCard = id_card;
	}
	
	@Override
	public String toString() {
		return "Market [id=" + id + ", id_buyer=" + idBuyer + ", id_seller=" + idSeller + ", id_card=" + idCard
				+ "]";
	}
	
	
}
