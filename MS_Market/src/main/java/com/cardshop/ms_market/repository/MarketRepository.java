package com.cardshop.ms_market.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.cardshop.ms_market.model.Market;

public interface MarketRepository extends CrudRepository <Market, Integer> {
	
	public List<Market> findAll();
	
	public Market findById(int id);
	
}
