package com.cardshop.ms_market.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cardshop.cardlib.dto.CardDTO;
import com.cardshop.cardlib.restclient.CardRestClient;
import com.cardshop.marketlib.dto.MarketDTO;
import com.cardshop.ms_market.dtomapper.MarketDTOMapper;
import com.cardshop.ms_market.model.Market;
import com.cardshop.ms_market.repository.MarketRepository;
import com.cardshop.userlib.dto.UserDTO;
import com.cardshop.userlib.restclient.UserRestClient;

@Service
@Transactional
public class MarketService {
	
	@Autowired
	MarketRepository mRepository;
	
	@Autowired
	UserRestClient uRestClient;
	
	@Autowired
	CardRestClient cRestClient;
	
	private MarketDTOMapper marketDTOMapper;
	
	public MarketService() {
		marketDTOMapper = new MarketDTOMapper();
	}
	
	@Transactional
	public Boolean sellCard(UserDTO user, CardDTO card) {
		
		UserDTO admin = uRestClient.findById(Integer.valueOf(0));
				//UserMapper.convertUserDto(uService.getUserById(Integer.valueOf(0)));
		
		// On crédite le vendeur
		user.setWallet(user.getWallet() + card.getPrix());
		
		// On soustrait l'admin 
		admin.setWallet(admin.getWallet() - card.getPrix());
		
		// On définit l'admin comme nouveau propriétaire
		card.setOwner(admin.getId());
		
		// Sauvegarde dans la BDD transaction
		Market market = new Market();
		market.setIdBuyer(admin.getId());
		market.setIdSeller(user.getId());
		market.setIdCard(card.getId());
		mRepository.save(market);
		// Envoie des modifs à card et user
		cRestClient.saveNewOwner(card.getId(), admin.getId());
		uRestClient.saveNewWallet(user.getId(), user.getWallet());
		uRestClient.saveNewWallet(admin.getId(), admin.getWallet());

		return true;
	}
	
	@Transactional
	public boolean buyCard(UserDTO user, CardDTO card) {
		
		UserDTO admin = uRestClient.findById(Integer.valueOf(0));
				//UserMapper.convertUserDto(uService.getUserById(Integer.valueOf(0)));
		
		// Si l'user a assez de fonds
		if(user.getWallet() > card.getPrix()) {
			
			// On soustrait son solde
			user.setWallet(user.getWallet() - card.getPrix());
			
			// On le définit comme nouveau owner
			card.setOwner(user.getId());
			
			// On crédite l'admin 
			admin.setWallet(admin.getWallet() + card.getPrix());
			
			// Sauvegarde dans la BDD transaction
			Market market = new Market();
			market.setIdBuyer(user.getId());
			market.setIdSeller(admin.getId());
			market.setIdCard(card.getId());
			mRepository.save(market);
			// Envoie des modifs à card et user
			cRestClient.saveNewOwner(card.getId(), user.getId());
			uRestClient.saveNewWallet(user.getId(), user.getWallet());
			uRestClient.saveNewWallet(admin.getId(), admin.getWallet());
			
			return true;
		}		
		return false;
	}
	
	public MarketDTO selectMarketById(int id) {
		return marketDTOMapper.getMarketDTO(mRepository.findById(id));
	}
	
	public List<MarketDTO> selectMarkets() {
		return mRepository.findAll()
				.stream()
				.map(market -> marketDTOMapper.getMarketDTO(market))
				.collect(Collectors.toList());
	}

}
