package com.cardshop.authlib.dto;

public class TokenDTO {
	private int id;
	private String token;
	private int userId;
	public TokenDTO() {
		
	}
	public TokenDTO(int id, String token, int userId) {
		super();
		this.id = id;
		this.token = token;
		this.userId = userId;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
}
