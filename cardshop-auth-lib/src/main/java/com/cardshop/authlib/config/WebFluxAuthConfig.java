package com.cardshop.authlib.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ClientHttpConnector;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;

import reactor.netty.http.client.HttpClient;

@Configuration
public class WebFluxAuthConfig
{

	private final String BASE_URL = "http://localhost:8084";
	
	@Bean
	public WebClient authWebClient()
	{
		HttpClient httpClient = HttpClient.create();
		
		ClientHttpConnector connector = new ReactorClientHttpConnector(httpClient.wiretap(true));

		return WebClient.builder()
		        .baseUrl(BASE_URL)
		        .clientConnector(connector)
		        .defaultHeader(
	        		HttpHeaders.CONTENT_TYPE,
	        		MediaType.APPLICATION_JSON_VALUE
        		)
		        .build();
	}
}
