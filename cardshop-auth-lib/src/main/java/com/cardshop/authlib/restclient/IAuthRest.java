package com.cardshop.authlib.restclient;


public interface IAuthRest {
	public String createToken(String login);
	public void checkToken(String encodedToken);
	public void disconnectUser(String encodedToken);
}
