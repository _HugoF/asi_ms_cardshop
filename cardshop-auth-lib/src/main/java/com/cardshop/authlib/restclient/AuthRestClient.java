package com.cardshop.authlib.restclient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;


@Service
public class AuthRestClient implements IAuthRest {

	@Autowired
	@Qualifier("authWebClient")
	WebClient restAuth;
	@Override
	public String createToken(String login) {
		LinkedMultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		map.add("login", login);
		return restAuth
			.post()
			.uri("/api/create")
			.body(BodyInserters.fromMultipartData(map))
			.retrieve()
			.bodyToMono(String.class).block();
	}

	@Override
	public void checkToken(String encodedToken) {
		restAuth
			.post()
			.uri("/api/check")
			.header("encoded-token", encodedToken);
	}

	@Override
	public void disconnectUser(String encodedToken) {
		restAuth
			.post()
			.uri("/api/logout")
			.header("encoded-token", encodedToken);
	}

}
