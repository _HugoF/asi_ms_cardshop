package com.cardshop.marketlib;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CardshopMarketLibApplication {

	public static void main(String[] args) {
		SpringApplication.run(CardshopMarketLibApplication.class, args);
	}

}
