package com.cardshop.marketlib.restclient;

import java.util.List;

import com.cardshop.marketlib.dto.ExchangeDTO;
import com.cardshop.marketlib.dto.MarketDTO;

public interface IMarketRest {
	
	List<MarketDTO> findAll();
	
	MarketDTO findById(int id);
	
	void sellCard(ExchangeDTO exchangeDTO);
	
	void buyCard(ExchangeDTO exchangeDTO);
}
