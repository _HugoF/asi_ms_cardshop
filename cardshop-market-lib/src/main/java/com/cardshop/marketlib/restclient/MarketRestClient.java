package com.cardshop.marketlib.restclient;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.reactive.function.client.WebClient;

import com.cardshop.marketlib.dto.ExchangeDTO;
import com.cardshop.marketlib.dto.MarketDTO;

import reactor.core.publisher.Mono;

public class MarketRestClient implements IMarketRest {
	
	@Autowired
	WebClient restClient;
	
	@Override
	@Qualifier("marketWebClient")
	public List<MarketDTO> findAll(){
		return restClient
				.get()
				.uri("/api/transactions")
				.retrieve()
				.bodyToFlux(MarketDTO.class)
				.collectList()
				.block();
	}
	
	@Override
	public MarketDTO findById(int id) {
		return restClient
				.get()
				.uri("/api/transactions/"+Integer.toString(id))
				.retrieve()
				.bodyToMono(MarketDTO.class)
				.block();
	}
	
	@Override
	public void sellCard(ExchangeDTO exchangeDTO) {
		restClient
			.post()
			.uri("/api/sell")
			.body(Mono.just(exchangeDTO), Integer.class);
	}
	
	@Override
	public void buyCard(ExchangeDTO exchangeDTO) {
		restClient
			.post()
			.uri("/api/buy")
			.body(Mono.just(exchangeDTO), Integer.class);
	}
}
