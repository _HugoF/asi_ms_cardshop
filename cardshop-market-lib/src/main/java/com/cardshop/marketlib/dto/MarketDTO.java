package com.cardshop.marketlib.dto;

public class MarketDTO {
	
	private int id;
	private int idBuyer;
	private int idSeller;
	private int idCard;
	
	public MarketDTO() {
	}
	
	public MarketDTO(int id, int idBuyer, int idSeller, int idCard) {
		super();
		this.id = id;
		this.idBuyer = idBuyer;
		this.idSeller = idSeller;
		this.idCard = idCard;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getIdBuyer() {
		return idBuyer;
	}
	
	public void setIdBuyer(int idBuyer) {
		this.idBuyer = idBuyer;
	}
	
	public int getIdSeller() {
		return idSeller;
	}
	
	public void setIdSeller(int idSeller) {
		this.idSeller = idSeller;
	}
	
	public int getIdCard() {
		return idCard;
	}
	
	public void setIdCard(int idCard) {
		this.idCard = idCard;
	}

	@Override
	public String toString() {
		return "MarketDTO [id=" + id + ", idBuyer=" + idBuyer + ", idSeller=" + idSeller + ", idCard=" + idCard + "]";
	}
}
