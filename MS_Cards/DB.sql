CREATE DATABASE card;
CREATE USER card with encrypted password 'password';
GRANT ALL PRIVILEGES ON DATABASE card to card;