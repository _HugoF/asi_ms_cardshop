package com.cardshop.card.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cardshop.card.mapper.CardMapper;
import com.cardshop.cardlib.dto.CardDTO;
import com.cardshop.card.repository.CardRepository;

@Service
public class CardService {
	
	@Autowired
	CardRepository cRepository;

	CardMapper cMapper = new CardMapper();

	public void addCard(CardDTO card) {
		cRepository.save(cMapper.convertCard(card));
	}

	public CardDTO getCard(int id) {
		Optional<CardDTO> hOpt = cRepository.findById(id).map(card -> cMapper.convertCardDto(card));
		if (hOpt.isPresent()) {
			return hOpt.get();
		} else {
			return null;
		}
	}
	
	public void saveNewOwner(int idNewOwner, int cardId) {
		cRepository.saveNewOwner(idNewOwner, cardId);
	}

}
