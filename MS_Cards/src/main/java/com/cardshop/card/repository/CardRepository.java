package com.cardshop.card.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.cardshop.card.model.Card;

public interface CardRepository extends CrudRepository<Card, Integer> {

	public List<Card> findByName(String name);
	
	@Modifying
	@Query(value = "UPDATE Card c SET c.owner=:idnewOwner WHERE c.id=:cardId")
	public void saveNewOwner(@Param("idnewOwner") int idnewOwner, @Param("cardId") int cardId);
}
