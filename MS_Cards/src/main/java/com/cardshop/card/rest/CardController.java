package com.cardshop.card.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cardshop.cardlib.dto.CardDTO;
import com.cardshop.cardlib.restclient.ICardRest;
import com.cardshop.card.service.CardService;

@RestController
public class CardController implements ICardRest{

	@Autowired
	CardService cService;
	
	@Override
	@RequestMapping(method = RequestMethod.POST, value = "/api/cards")
	public void addCard(@RequestBody CardDTO card) {
		cService.addCard(card);
	}
	
	@Override
	@RequestMapping(method = RequestMethod.GET, value = "/api/cards/{id}")
	public CardDTO findById(@PathVariable int id) {
		return cService.getCard(id);
	}
	
	@Override
	@RequestMapping(method = RequestMethod.POST, value = "/api/cards/{idCard}")
	public void saveNewOwner(@PathVariable int idCard, @RequestBody int idNewOwner) {
		cService.saveNewOwner(idNewOwner, idCard);
	}

}
