package com.cardshop.cardlib.restclient;

import com.cardshop.cardlib.dto.CardDTO;

public interface ICardRest {
	
	void saveNewOwner(int idCard, int idNewOwner);
	
	CardDTO findById(int id);
	
	void addCard(CardDTO card);
}
