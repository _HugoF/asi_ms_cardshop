package com.cardshop.cardlib.restclient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import com.cardshop.cardlib.dto.CardDTO;

import reactor.core.publisher.Mono;

@Service
public class CardRestClient implements ICardRest {

	@Autowired
	@Qualifier("cardWebClient")
	WebClient restClient;

	@Override
	public void saveNewOwner(int idCard, int idNewOwner) {
		restClient
		.post()
		.uri("/api/cards/"+Integer.toString(idCard))
		.body(Mono.just(idNewOwner), Integer.class);
	}

	@Override
	public CardDTO findById(int id) {
		return restClient
				.get()
				.uri("/api/cards/"+Integer.toString(id))
				.retrieve()
				.bodyToMono(CardDTO.class)
				.block();
	}
	
	@Override
	public void addCard(CardDTO card) {
		restClient
		.post()
		.uri("/api/cards")
		.body(Mono.just(card), CardDTO.class);
	}
	
}
