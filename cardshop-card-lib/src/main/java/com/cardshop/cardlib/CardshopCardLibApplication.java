package com.cardshop.cardlib;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CardshopCardLibApplication {

	public static void main(String[] args) {
		SpringApplication.run(CardshopCardLibApplication.class, args);
	}

}
