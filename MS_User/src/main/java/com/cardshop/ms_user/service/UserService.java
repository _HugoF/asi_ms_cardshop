package com.cardshop.ms_user.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cardshop.ms_user.dtomapper.UserDTOMapper;
import com.cardshop.ms_user.repository.UserRepository;
import com.cardshop.userlib.dto.UserDTO;

/**
 * 
 * @author Hugo Ferrer
 *
 */

@Service
public class UserService {

	@Autowired
	UserRepository userRepository;
	
	private UserDTOMapper userDTOMapper;
	
	/**
	 * Constructor of UserService
	 */
	public UserService() {
		userDTOMapper = new UserDTOMapper();
	}
	
	/**
	 * Allow the client to save a user in the database
	 * @param userDTO
	 */
	public void insertUser(UserDTO userDTO) {
		userRepository.save(
			userDTOMapper.getUser(userDTO)
		);
	}
	
	/**
	 * Allow the client to retrieve all the users in the database
	 * @return List<UserDTO>
	 */
	public List<UserDTO> selectUsers() {
		return userRepository.findAll()
				.stream()
				.map(user -> userDTOMapper.getUserDTO(user))
				.collect(Collectors.toList());
	}
	
	/**
	 * Allow the client to retrieve a user with its id
	 * @param id
	 * @return
	 */
	public UserDTO selectUserById(int id) {
		return userDTOMapper.getUserDTO(userRepository.findById(id));
	}
	
	/**
	 * Allow the client to retrieve a user with its login
	 * @param login
	 * @return
	 */
	public UserDTO selectUserByLogin(String login) {
		return userDTOMapper.getUserDTO(userRepository.findByLogin(login).get());
	}
	
	public void saveNewWallet(int id, int newWallet) {
		UserDTO userDTO = selectUserById(id);
		userDTO.setWallet(newWallet);
		userRepository.save(
				userDTOMapper.getUser(userDTO)
			);
	}
	
}
