package com.cardshop.ms_user.restcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cardshop.ms_user.service.UserService;
import com.cardshop.userlib.dto.UserDTO;
import com.cardshop.userlib.restclient.IUserRest;

/**
 * 
 * @author Hugo Ferrer
 *
 */

@RestController
public class UserRestController implements IUserRest {

	@Autowired
	UserService userService;
	
	@Override
	@RequestMapping(method=RequestMethod.POST, value="/api/users")
	public void save(@RequestBody UserDTO userDTO) {
		userService.insertUser(userDTO);
	}
	
	@Override
	@RequestMapping(method=RequestMethod.POST, value="/api/users/{id}")
	public void saveNewWallet(@PathVariable int id, @RequestBody int newWallet) {
		userService.saveNewWallet(id, newWallet);
	}
	
	@Override
	@RequestMapping(method=RequestMethod.GET, value="/api/users")
	public List<UserDTO> findAll() {
		return userService.selectUsers();
	}

	@Override
	@RequestMapping(method=RequestMethod.GET, value="/api/users/{id}")
	public UserDTO findById(@PathVariable int id) {
		return userService.selectUserById(id);
	}

	@Override
	@RequestMapping(method=RequestMethod.GET, value="/api/users/{login}")
	public UserDTO findByLogin(@PathVariable String login) {
		return userService.selectUserByLogin(login);
	}

	
	
}
