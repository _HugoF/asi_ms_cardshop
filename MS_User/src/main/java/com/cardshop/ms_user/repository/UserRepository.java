package com.cardshop.ms_user.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.cardshop.ms_user.model.User;

public interface UserRepository extends CrudRepository<User, Integer> {

	public List<User> findAll();
	
	public User findById(int id);
	
	public Optional<User> findByLogin(String login);
	
}
