package com.cardshop.ms_user.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * 
 * @author Hugo Ferrer
 *
 */

@Entity
@Table(name = "users")
public class User {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="userseq")
	@SequenceGenerator(name = "userseq", sequenceName = "userseq", allocationSize = 1)
	private int id;
	private String name;
	private int wallet;
	private String login;
	private String password;
		
	public User() {
		
	}
	
	public User(int id, String name, int wallet, String login, String password) {
		super();
		this.id = id;
		this.name = name;
		this.wallet = wallet;
		this.login = login;
		this.password = password;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getWallet() {
		return wallet;
	}
	
	public void setWallet(int wallet) {
		this.wallet = wallet;
	}
	
	public String getLogin() {
		return login;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}

}
