package com.cardshop.ms_user.dtomapper;

import com.cardshop.ms_user.model.User;
import com.cardshop.userlib.dto.UserDTO;

public class UserDTOMapper {

	public UserDTOMapper() {}
	
	public User getUser(UserDTO userDTO) {
		return new User(userDTO.getId(), userDTO.getName(), userDTO.getWallet(), userDTO.getLogin(), userDTO.getPassword());
	}
	
	public UserDTO getUserDTO(User user) {
		return new UserDTO(user.getId(), user.getName(), user.getWallet(), user.getLogin(), user.getPassword());
	}
	
}
