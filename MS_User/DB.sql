CREATE DATABASE users;
CREATE USER users with encrypted password 'password';
GRANT ALL PRIVILEGES ON DATABASE users to users;