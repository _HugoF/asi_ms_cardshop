package com.cardshop.userlib.restclient;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import com.cardshop.userlib.dto.UserDTO;

import reactor.core.publisher.Mono;

/**
 * 
 * @author Hugo Ferrer
 *
 */

@Service
public class UserRestClient implements IUserRest {

	@Autowired
	@Qualifier("userWebClient")
	WebClient restClient;
	
	@Override
	public void saveNewWallet(int id, int NewWallet) {
		restClient
		.post()
		.uri("/api/users/"+Integer.toString(id))
		.body(Mono.just(NewWallet), Integer.class);
	}
	
	@Override
	public void save(UserDTO user) {
		restClient
			.post()
			.uri("/api/users")
			.body(Mono.just(user), UserDTO.class);
	}
	
	@Override
	public List<UserDTO> findAll() {
		return restClient
				.get()
				.uri("/api/users")
				.retrieve()
				.bodyToFlux(UserDTO.class)
				.collectList()
				.block();
	}

	@Override
	public UserDTO findById(int id) {
		return restClient
				.get()
				.uri("/api/users/"+Integer.toString(id))
				.retrieve()
				.bodyToMono(UserDTO.class)
				.block();
	}

	@Override
	public UserDTO findByLogin(String login) {
		return restClient
				.get()
				.uri("/api/users/"+login)
				.retrieve()
				.bodyToMono(UserDTO.class)
				.block();
	}
	
	
	
}
