package com.cardshop.userlib.restclient;

import java.util.List;

import com.cardshop.userlib.dto.UserDTO;

/**
 * 
 * @author Hugo Ferrer
 *
 */

public interface IUserRest {
	
	void save(UserDTO user);

	List<UserDTO> findAll();
	
	UserDTO findById(int id);
	
	UserDTO findByLogin(String login);

	void saveNewWallet(int id, int newWallet);
	
}
